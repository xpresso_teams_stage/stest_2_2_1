from xpresso.ai.core.commons.network.http.send_request import SendHTTPRequest
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.utils.constants import CONTROLLER_FIELD, \
    SERVER_URL_FIELD, MONGO_DOC_FILTER, MONGO_PROJECTION_FILTER, \
    MONGO_UPDATE_FLAG, MONGO_UPDATE_INFO
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    ControllerClientResponseException, ControllerAPICallException
from xpresso.ai.core.data.pipeline.ExperimentFieldName import ExperimentFieldName


class APIUtils:
    """
    utils class with useful api calls to xpresso controller
    """
    def __init__(self, config: XprConfigParser = XprConfigParser()):
        """
        constructor method with default attributes setup
        """
        self.config = config
        server_url = self.config[CONTROLLER_FIELD][SERVER_URL_FIELD]
        self.server_endpoint_run = f"{server_url}/run/api"
        self.server_endpoint_experiment = f"{server_url}/exp/api"
        self.server_endpoint_project = f"{server_url}/project/api"
        self.server_endpoint_decode_run = f"{server_url}/run/decode"
        self.server_endpoint_dv_branch = f"{server_url}/dv_branch/api"
        self.request_module = SendHTTPRequest()
        self.logger = XprLogger()

    def get_run_info(self, run_filter: dict):
        """
        get the run info from database

        Args:
            run_filter: dictionary to filter the run from the collection
        """
        try:
            response = self.request_module.send(
                self.server_endpoint_run, HTTPMethod.GET, run_filter
            )
            return response
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def update_run_info(self, update_info: dict):
        """
        updates run information on database

        Args:
            update_info: info that needs to be modified on db
        """
        try:
            self.request_module.send(
                self.server_endpoint_run, HTTPMethod.PUT, update_info
            )
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def get_experiment_info(self, experiment_filter: dict):
        """
        fetches the information of experiment from database

        Args:
            experiment_filter: dictionary to filter out the experiments
            from collection
        Returns:
            returns the information of experiment from db as a list
        """
        try:
            response_expt = self.request_module.send(
                self.server_endpoint_experiment, HTTPMethod.GET,
                experiment_filter
            )
            return response_expt
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def get_project_info(self, project_filter: dict):
        """
        fetches the project information from database

        Args:
            project_filter: information to filter the projects from database

        Returns:
            returns the project info as a list
        """
        try:
            response_project = self.request_module.send(
                self.server_endpoint_project, HTTPMethod.GET, project_filter
            )
            return response_project
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def decode_xpresso_run_name(self, xpresso_run_name: str) -> dict:
        """
        sends a request to decode xpresso_run_name and fetch appropriate
        info from that

        Args:
            xpresso_run_name: xpresso generated run name
        Returns:
             info on run as a dictionary
        """
        try:
            decode_info = self.request_module.send(
                self.server_endpoint_decode_run, HTTPMethod.GET,
                {
                    ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name
                }
            )
            return decode_info
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def add_branch_record(self, branch_info: dict):
        """
        add a new record in branches collection

        Args:
            branch_info: info of the branch that needs to be added in database
        """
        self.send_request(
            self.server_endpoint_dv_branch, HTTPMethod.POST, branch_info
        )

    def get_branch_info(self, branch_filter: dict, projection_filter: dict):
        """
        fetch the information of branches from database

        Args:
            branch_filter: info to filter out the branch
            projection_filter: info of the fields that needs
            to be fetched from the record
        """
        return self.send_request(
            self.server_endpoint_dv_branch, HTTPMethod.GET,
            {
                MONGO_DOC_FILTER: branch_filter,
                MONGO_PROJECTION_FILTER: projection_filter
            }
        )

    def update_branch_info(self, branch_filter: dict, update_info: dict,
                           update_flag: str):
        """
        update branch info on database

        Args:
            branch_filter: info to filter out the branch
            update_info: info of the branch that needs to be updated
            update_flag: modify flag i.e. $set, $push
        """
        self.send_request(
            self.server_endpoint_dv_branch, HTTPMethod.PUT,
            {
                MONGO_DOC_FILTER: branch_filter,
                MONGO_UPDATE_INFO: update_info,
                MONGO_UPDATE_FLAG: update_flag
            }
        )

    def delete_branch_record(self, branch_filter: dict):
        """
        delete branch record from the database

        Args:
            branch_filter: info to filter out the branch from database
        """
        self.send_request(
            self.server_endpoint_dv_branch, HTTPMethod.DELETE, branch_filter)

    def send_request(self, endpoint: str, request_method: HTTPMethod,
                     request_info: dict):
        """
        send requests to xpresso controller.

        Args:
            endpoint: url to hit on the xpresso controller
            request_method: type of request method
            request_info: request body that needs to be sent
        """
        try:
            response = self.request_module.send(
                endpoint, request_method, request_info
            )
            return response
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)
